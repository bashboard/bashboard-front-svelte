# Bashboard frontend

- Backend is [here](https://gitlab.com/bashboard/bashboard-backend)
- Built using [Sapper](https://sapper.svelte.dev/) and [Svelte](https://svelte.dev/).

## Run dev server

```
echo "BACKEND_URI=http://localhost:4001" >> .env
npm i
npm start
```

## Deploy production

- Deploy [backend](https://gitlab.com/bashboard/bashboard-backend) first.
- Follow backend steps, just replace "back" with "front"
- `echo "BACKEND_URI=http://localhost:4000" >> /var/lib/bashboard/front/.env`
- `chown -R web:web /var/lib/bashboard`
- Run Gitlab CD

