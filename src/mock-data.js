export const barChart = {
    meta: {
        transform: null,
        timeseries: "series",
        namespace: ["bashboard", "system", "Uptime-(hours)"]
    },
    data: [
        {
            datetime: "2020-04-27T08:53:10Z",
            body: 0.31
        },
        {
            datetime: "2020-04-27T08:53:20Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:53:30Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:53:40Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:53:50Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:54:00Z",
            body: 0.33
        },
        {
            datetime: "2020-04-27T08:53:10Z",
            body: 0.31
        },
        {
            datetime: "2020-04-27T08:53:20Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:53:30Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:53:40Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:53:50Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:54:00Z",
            body: 0.33
        },
        {
            datetime: "2020-04-27T08:53:10Z",
            body: 0.31
        },
        {
            datetime: "2020-04-27T08:53:20Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:53:30Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:53:40Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:53:50Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:54:00Z",
            body: 0.33
        },
        {
            datetime: "2020-04-27T08:53:10Z",
            body: 0.31
        },
        {
            datetime: "2020-04-27T08:53:20Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:53:30Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:53:40Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:53:50Z",
            body: 0.32
        },
        {
            datetime: "2020-04-27T08:54:00Z",
            body: 0.33
        }
    ]
};

export const lastChart = {
    meta: {
        transform: null,
        timeseries: "last",
        namespace: ["bashboard", "system", "Uptime-(hours)"]
    },
    data: [
        {
            datetime: "2020-04-27T08:53:10Z",
            body: 0.31
        }
    ]
};

export const lastChartObj = {
    meta: {
        transform: null,
        timeseries: "last",
        namespace: ["bashboard", "stack", "example-sale"]
    },
    data: [
        {
            datetime: "2020-04-27T09:13:55Z",
            body: {
                price: 5,
                amount: 10,
                age: 45
            }
        }
    ]
};

export const stackChart = {
    meta: {
        transform: null,
        timeseries: "series",
        namespace: ["bashboard", "stack", "example-sale"]
    },
    data: [
        {
            datetime: "2020-04-27T09:13:55Z",
            body: {
                price: 5,
                amount: 12,
                age: 45
            }
        },
        {
            datetime: "2020-04-27T09:14:05Z",
            body: {
                price: 50,
                amount: 1,
                age: 11
            }
        },
        {
            datetime: "2020-04-27T09:15:16Z",
            body: {
                price: 5,
                amount: 10,
                age: 45
            }
        }
    ]
};

export const stackChartIrregular = {
    meta: {
        transform: null,
        timeseries: "series",
        namespace: ["bashboard", "stack", "example-sale"]
    },
    data: [
        {
            datetime: "2020-02-22T11:29:12Z",
            body: {
                test: true,
                deploy: "1",
                build: "1"
            }
        },
        {
            datetime: "2020-02-22T11:31:15Z",
            body: {
                deploy: "1",
                build: "1"
            }
        },
        {
            datetime: "2020-02-22T11:36:23Z",
            body: {
                deploy_time: "24",
                build_time: "57"
            }
        },
        {
            datetime: "2020-02-22T11:44:48Z",
            body: {
                deploy_time: "23",
                build_time: "7"
            }
        },
        {
            datetime: "2020-02-22T11:50:06Z",
            body: {
                deploy_time: "24",
                build_time: "5"
            }
        },
        {
            datetime: "2020-03-02T15:04:20Z",
            body: {
                deploy_time: "20",
                build_time: "19"
            }
        },
        {
            datetime: "2020-03-02T15:25:07Z",
            body: {
                deploy_time: "24",
                build_time: "81"
            }
        },
        {
            datetime: "2020-03-02T15:31:07Z",
            body: {
                deploy_time: "24",
                build_time: "81"
            }
        },
        {
            datetime: "2020-03-09T15:40:55Z",
            body: {
                deploy_time: "24",
                build_time: "66"
            }
        },
        {
            datetime: "2020-03-12T14:54:50Z",
            body: {
                deploy_time: "20",
                build_time: "12"
            }
        },
        {
            datetime: "2020-03-12T15:15:02Z",
            body: {
                deploy_time: "25",
                build_time: "164"
            }
        },
        {
            datetime: "2020-03-12T15:26:13Z",
            body: {
                deploy_time: "24",
                build_time: "79"
            }
        },
        {
            datetime: "2020-03-13T09:10:47Z",
            body: {
                deploy_time: "25",
                build_time: "79"
            }
        },
        {
            datetime: "2020-03-13T11:30:46Z",
            body: {
                deploy_time: "27",
                build_time: "94"
            }
        },
        {
            datetime: "2020-03-13T12:02:26Z",
            body: {
                deploy_time: "25",
                build_time: "69"
            }
        },
        {
            datetime: "2020-03-24T09:28:39Z",
            body: {
                deploy_time: "24",
                build_time: "92"
            }
        },
        {
            datetime: "2020-03-24T09:33:44Z",
            body: {
                deploy_time: "24",
                build_time: "5"
            }
        },
        {
            datetime: "2020-03-24T10:30:07Z",
            body: {
                deploy_time: "24",
                build_time: "77"
            }
        },
        {
            datetime: "2020-03-24T11:13:48Z",
            body: {
                deploy_time: "24",
                build_time: "3"
            }
        },
        {
            datetime: "2020-04-22T16:29:53Z",
            body: {
                deploy_time: "29",
                build_time: "0"
            }
        },
        {
            datetime: "2020-04-22T16:29:54Z",
            body: {
                deploy_time: "29",
                build_time: "1"
            }
        },
        {
            datetime: "2020-04-22T16:29:56Z",
            body: {
                deploy_time: "29",
                build_time: "0"
            }
        },
        {
            datetime: "2020-04-23T09:50:33Z",
            body: {
                deploy_time: "23",
                build_time: "2147"
            }
        },
        {
            datetime: "2020-04-23T10:51:45Z",
            body: {
                deploy_time: "23",
                build_time: "66"
            }
        },
        {
            datetime: "2020-04-23T11:30:31Z",
            body: {
                deploy_time: "23",
                build_time: "147"
            }
        },
        {
            datetime: "2020-04-24T08:51:23Z",
            body: {
                deploy_time: "23",
                build_time: "63"
            }
        },
        {
            datetime: "2020-04-24T22:47:35Z",
            body: {
                deploy_time: "27",
                build_time: "0"
            }
        },
        {
            datetime: "2020-04-24T22:47:41Z",
            body: {
                deploy_time: "29",
                build_time: "3"
            }
        },
        {
            datetime: "2020-04-24T22:47:43Z",
            body: {
                deploy_time: "24",
                build_time: "1"
            }
        }
    ]
};

export default { barChart, lastChart, lastChartObj, stackChart, stackChartIrregular }
