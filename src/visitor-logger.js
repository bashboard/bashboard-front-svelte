import { v4 as uuidv4 } from 'uuid';

export default async function () {
    fetch(`${BACKEND_URI}/bashboard/landing/page-visits`, {
        method: 'POST',
        body: JSON.stringify({
            [location.pathname]: 1
        })
    })

    const cid = localStorage.getItem("cid")
    if (!cid) {
        fetch(`${BACKEND_URI}/bashboard/landing/unique-visits`, {
            method: 'POST',
            body: JSON.stringify(1)
        })
        const ref = document.referrer.split('/')[2] || "direct"
        fetch(`${BACKEND_URI}/bashboard/landing/referrer`, {
            method: 'POST',
            body: JSON.stringify({
                [ref]: 1
            })
        })
        localStorage.setItem("cid", uuidv4())
    }
}
